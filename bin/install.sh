#!/usr/bin/env bash

set -eu
set -o pipefail

bindir=$(realpath "$(dirname "$0")")
rootdir="$bindir"/..

. "$bindir"/functions.bash

USAGE="Usage: $0 [-f]"

dotfiles=(
  bashrc
  bashrc-user
  screenrc
  inputrc
  fzf.bash
)

opt_f=
while getopts hf-: arg; do
  case $arg in
    h) warn "$USAGE"; exit 0 ;;
    f) opt_f=yes ;;
    -)  OPTARG_KEY="${OPTARG%=*}"
      OPTARG_VALUE="${OPTARG#*=}"
      case $OPTARG_KEY in
        help)  warn "$USAGE"; exit 0 ;;
        '')  break ;;
        *)   error "Illegal option --$OPTARG" ;;
        esac ;;
    *) error "$USAGE" ;;
  esac
done
shift $((OPTIND-1))

go() {
  local dotfile
  for dotfile in "${dotfiles[@]}"; do
    cmd ln -sf "$rootdir"/"$dotfile" ."$dotfile"
  done
}

check-exists() {
  local dir=$1
  local found=
  for dotfile in "${dotfiles[@]}"; do
    local f="$dir"/."$dotfile"
    if [ -e "$f" ]; then
      warn "$f" exists
      found=yes
    fi
  done
  if [ -z "$found" ]; then
    return 1
  fi
}

if [ ! "$opt_f" = yes ] && check-exists "$HOME"; then
  error "Use -f to install"
fi
cwd "$HOME" go
