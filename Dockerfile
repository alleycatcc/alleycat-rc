FROM debian:bookworm-20240110

# we change the default shell to bash, in order to use 'source'
SHELL ["/bin/bash", "-c"]

RUN apt update

RUN apt install --no-install-recommends -y \
  ca-certificates curl gnupg

RUN mkdir -p /etc/apt/keyrings

RUN curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg

RUN echo 'deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_20.x nodistro main' > /etc/apt/sources.list.d/nodesource.list

RUN apt update
RUN apt install --no-install-recommends -y \
    aptitude git \
    nodejs vim-gtk3 screen fzf

# create user 'user', add to sudo group (to use git, apt, etc)
RUN useradd --create-home --shell /bin/bash user && \
    adduser user sudo

USER user

WORKDIR /home/user

RUN git clone https://gitlab.com/alleycatcc/alleycat-rc && \
    cd alleycat-rc && \
    git submodule update --init --recursive

RUN git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

RUN ln -sf ~/alleycat-rc/bashrc ~/.bashrc && \
    ln -sf ~/alleycat-rc/bashrc-user ~/.bashrc-user && \
    ln -sf ~/alleycat-rc/fzf.bash ~/.fzf.bash && \
    ln -sf ~/alleycat-rc/screenrc ~/.screenrc && \
    ln -sf ~/alleycat-rc/vim/vimrc ~/.vim/vimrc

RUN source ~/.bashrc

